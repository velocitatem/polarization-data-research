import csv
import matplotlib.pyplot as plt
import numpy as np

c = open('multiTimeline.csv','r')
o = csv.reader(c)
data = [g for g in o][4:]


delta_time = 2 # points


def clean_metric(date):
    return int(date.replace("<","").replace(">",""))



result_ranges = []
football_metrics, politics_metrics = list(), list()
timeline = list() # could be range, here but we need to compare times in the loop

def metrics_for_point(point):
    football_metric, politics_metric = clean_metric(point[1]), clean_metric(point[2])
    return (football_metric, politics_metric)

for (point, i) in zip(data, range(len(data)-delta_time)):
    metrics_now = metrics_for_point(point)
    metrics_after_shift = metrics_for_point(data[i+delta_time])
    timeline.append(i)
    football_metrics.append(metrics_now[0])
    politics_metrics.append(metrics_now[1])
    delta_politics, delta_football = (metrics_after_shift[0]-metrics_now[0])/delta_time, (metrics_after_shift[1]-metrics_now[1])/delta_time
    if (delta_politics > 0 and delta_football < 0) or (delta_football > 0 and delta_politics < 0):
        print(point[0], i)
        result_ranges.append([[i,metrics_now[0]], [i+delta_time,metrics_after_shift[0]]])
        result_ranges.append([[i,metrics_now[1]], [i+delta_time,metrics_after_shift[1]]])

plt.plot(timeline, football_metrics)
plt.plot(timeline, politics_metrics)
for rng in result_ranges:
    print(rng)
    rng=np.array(rng).T
    plt.fill_between(rng[0], rng[1])
plt.show()

    




