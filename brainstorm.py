import csv
import matplotlib.pyplot as plt
import numpy as np


c = open('multiTimeline.csv','r')
o = csv.reader(c)
i=0
o = [g for g in o]
o = o[4:]
m=len(o)

fl = []
el = []
olx = []
oly = []

dt=14

for r in o:
    r[1] = r[1].replace("<","").replace(">","")
    r[2] = r[2].replace("<","").replace(">","")
    fl.append([i, int(r[1])])
    el.append([i+0.001, int(r[2])])
    if i < m-dt:
        d1=int(r[1])-int(o[i+dt][1].replace("<","").replace(">","")) # football  d
        d2=int(r[2])-int(o[i+dt][2].replace("<","").replace(">","")) # elections d
        if d2 < 0 and d1 > 0:
            print(r[0])
            olx.append(i-dt/2)
            oly.append(int(r[1]))
            olx.append(i-dt/2)
            oly.append(int(r[2]))
    i+=1
c.close()

plt.plot(fl)
plt.plot(el)
plt.scatter(olx,oly)
plt.show()
